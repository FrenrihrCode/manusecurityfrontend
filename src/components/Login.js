import React, { useState, useEffect } from 'react';
import * as api from '../api/api';
import { saveToken, isAuthed } from "../api/tokenUtils";
import { Redirect } from 'react-router-dom';

export default function Login(props) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        const authed = isAuthed();
        if(authed) setRedirect(true)
    }, []);

    const handleFormSubmit = async (event) => {
        event.preventDefault();
        if (username && password) {
            try {
                const payload = { email: username, password: password };
                const { data } = await api.signin(payload);
                if (data.ok) {
                    saveToken(data.token);
                    const { from } = props.location.state || {
                        from: { pathname: "/" }
                    };
                    props.history.push(from.pathname);
                } else{
                    setError(data.message)
                }
            } catch (error) {
                console.log('err',error);
            }
        }
    };

    return (
        redirect ? <Redirect to='/' />
        :   <div className="container">
                <div className="container">
                    <div className="container">
                        <div className="col s6">
                            <div className="card-panel hoverable">
                                <center>
                                    <form className="col s12" onSubmit={handleFormSubmit}>
                                        <div className="input-field col s12 center" style={{ paddingBottom: 5 }}>
                                            <h5 className="center login-form-text">Ingresar a la sala</h5>
                                        </div>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">account_circle</i>
                                            <input id="icon_prefix" type="text" value={username} className="validate" onChange={(event) => setUsername(event.target.value)} />
                                            <label htmlFor="icon_prefix">Nombre de usuario</label>
                                            <span className="red-text" >{error}</span>
                                        </div>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">lock</i>
                                            <input id="icon_telephone" type="password" value={password} className="validate" onChange={(event) => setPassword(event.target.value)} />
                                            <label htmlFor="icon_telephone">Contraseña</label>
                                            <span className="red-text" >{error}</span>
                                        </div>
                                        <div className="input-field col s12">
                                            <button className="btn waves-effect waves-light" type="submit" name="action">INGRESAR
                                                <i className="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </form>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}
