import React from "react";
import { Route } from "react-router-dom";
import { isAuthed } from "../api/tokenUtils";

const AuthRoute = ({
  authComponent: Navigation2,
  unAuthComponent: Navigation,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      const authedId = isAuthed();
      return authedId ? (
        <Navigation2 authedId={authedId} {...props} />
      ) : (
        <Navigation {...props} />
      );
    }}
  />
);

export default AuthRoute;