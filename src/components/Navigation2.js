import React from 'react';
import { Link } from 'react-router-dom';
import { removeToken } from "../api/tokenUtils";
/*
componentDidMount() {
        const script = document.createElement("script");
        const scriptText = document.createTextNode("document.addEventListener('DOMContentLoaded', function() {"
            + " var elems = document.querySelectorAll('.sidenav');"
            + " var instances = M.Sidenav.init(elems, {});"
            + " });");
        script.appendChild(scriptText);
        document.head.appendChild(script);
    }
    */
const Navigation2 = ({ history, authedId }) => (
    <>
        <nav>
            <div className="nav-wrapper grey darken-4">
                <Link to="/" className="brand-logo" style={{ paddingLeft: 15 }}>
                    ChatApp
                </Link>
                <Link to="/" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></Link>
                <ul className="right hide-on-med-and-down">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/salas">Salas</Link></li>
                    <li><Link to="/gestionsalas">Administrar salas</Link></li>
                    <li><button className='waves-effect waves-light btn' onClick={() => {
                        removeToken();
                        history.push("/");
                    }}>Cerrar Sesión</button></li>
                </ul>
            </div>
        </nav>

        <ul className="sidenav center" id="mobile-demo">
            <li><Link to="/">Home</Link></li>
            <li><Link to="/salas">Salas</Link></li>
            <li><Link to="/gestionsalas">Administrar salas</Link></li>
            <li><button className='waves-effect waves-light btn' onClick={() => {
                removeToken();
                history.push("/");
            }}>Cerrar Sesión</button></li>
        </ul>
    </>
);

export default Navigation2;
