import React, { Component } from 'react'

export default class Home extends Component {
    render() {
        return (
            <div className="container">
                <br></br>
                <div className="container">
                    <div className="container">
                        <div className="col">
                            <div className="card-panel hoverable grey lighten-3">
                                <center>
                                    <div className="col s5">
                                        <h1 className="blue-text">Bienvenido</h1>
                                        <img src="https://i.pinimg.com/originals/e3/1b/75/e31b752875679b64fce009922f9f0dda.gif" alt='home' style={{ width: '75%' }}></img>
                                        <h4>Disfruta y comparte experiencias únicas</h4>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
