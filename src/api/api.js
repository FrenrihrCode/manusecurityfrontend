import axios from "axios";
import { getToken } from "./tokenUtils";

const SERVER_URL = 'https://practica-04-dawa.herokuapp.com/';
axios.defaults.baseURL = SERVER_URL;

const setAuthHeader = () => ({
  headers: { authorization: `Bearer ${getToken()}` }
});

export const signup = payload => axios.post(`/api/users/register`, payload);
export const signin = payload => axios.post(`/api/users/login`, payload);
export const getUser = userId => axios.get(`/api/users/${userId}`, setAuthHeader());
export const reportes = payload => axios.post('/api/reports/newreport', payload);
export const reporteslistar = () => axios.get('/api/reports/');
export const reportesget= Id => axios.get(`/api/reports/${Id}`);
export const reportesput= (Id, payload) => axios.put(`/api/reports/${Id}`, payload);
export const reportesdelete= Id => axios.delete(`/api/reports/${Id}`);