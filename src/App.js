import React from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.css';
import Navigation from './components/Navigation';
import Navigation2 from './components/Navigation2';
import AuthRoute from './components/AuthRouter';
import ListSalas from './components/ListSalas';
import PrivateRoute from './components/PrivateRoute';
import gestionSalas from './components/gestionSala';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import Chat from './components/Chat';
import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.js';

function App() {
  return (
    <Router>
      <AuthRoute
            path="/"
            authComponent={Navigation2}
            unAuthComponent={Navigation}
          />
      <Switch>
        <Route path='/' exact component={Home}/>
        <Route path='/login' component={Login}/>
        <Route path='/register' component={Register}/>
        <PrivateRoute path='/chat' component={Chat}/>
        <PrivateRoute path='/salas' component={ListSalas}/>
        <PrivateRoute path='/gestionsalas' component={gestionSalas}/>
      </Switch>
    </Router>
  );
}

export default App;
