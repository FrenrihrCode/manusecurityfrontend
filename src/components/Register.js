import React, { useState, useEffect } from 'react';
import * as api from '../api/api';
import { saveToken, isAuthed } from "../api/tokenUtils";
import { Redirect } from 'react-router-dom';

export default function Register(props) {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [error, setError] = useState('');
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        const authed = isAuthed();
        if(authed) setRedirect(true)
    }, []);

    const handleFormSubmit = async (event) => {
        event.preventDefault();
        if (username && password && email && password2) {
            if (password!==password2){
                setError('Las contraseñas no coinciden.')
            }else{
                try {
                    const payload = { username: username, email: email, password: password };
                    const { data } = await api.signup(payload);
                    if (data.ok) {
                        saveToken(data.token);
                        const { from } = props.location.state || {
                            from: { pathname: "/" }
                        };
                        props.history.push(from.pathname);
                    } else{
                        setError(data.err)
                    }
                } catch (error) {
                    console.log('err',error);
                }
            }
        }
    };

    return (
        redirect ? <Redirect to='/' />
        :   <div className="container">
                <div className="container">
                    <div className="container">
                        <div className="col s6">
                            <div className="card-panel hoverable">
                                <center>
                                    <form className="col s12" onSubmit={handleFormSubmit}>
                                        <div className="input-field col s12 center" style={{ paddingBottom: 5 }}>
                                            <h5 className="center login-form-text">Ingresar a la sala</h5>
                                        </div>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">account_circle</i>
                                            <input id="u" type="text" value={username} className="validate" onChange={(event) => setUsername(event.target.value)} />
                                            <label htmlFor="u">Nombre de usuario</label>
                                            <span className="red-text" >{error}</span>
                                        </div>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">account_circle</i>
                                            <input id="a" type="text" value={email} className="validate" onChange={(event) => setEmail(event.target.value)} />
                                            <label htmlFor="a">Email</label>
                                            <span className="red-text" >{error}</span>
                                        </div>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">lock</i>
                                            <input id="s" type="password" value={password} className="validate" onChange={(event) => setPassword(event.target.value)} />
                                            <label htmlFor="s">Contraseña</label>
                                            <span className="red-text" >{error}</span>
                                        </div>
                                        <div className="input-field col s12">
                                            <i className="material-icons prefix">lock</i>
                                            <input id="d" type="password" value={password2} className="validate" onChange={(event) => setPassword2(event.target.value)} />
                                            <label htmlFor="d">Confirmar contraseña</label>
                                            <span className="red-text" >{error}</span>
                                        </div>
                                        <div className="input-field col s12">
                                            <button className="btn waves-effect waves-light" type="submit" name="action">INGRESAR
                                                <i className="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </form>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}
