import React, { Component } from 'react';
import * as api from '../api/api';
import { Link } from 'react-router-dom';

export default class ListSalas extends Component {
    constructor(){
        super();
        this.state={
            salas:[]
        };
    }
    componentDidMount(){
        this.listarPelicula();
    }
    async listarPelicula(){
        try{
            const {data} = await api.reporteslistar();
            this.setState({salas: data});
            console.log(data);
        } catch (error) {
            console.log("error" + error);
        }
    }

    render() {
        return (
            <div className="container">
                  <div className="col s12 m7">
                    <h2 className="header">Lista de Salas</h2>
                    <div className="row">
                    {
                        this.state.salas.map(sala=>{
                          return(
                            <div className="col s12 m6" key={sala._id}>
                                <div className="card horizontal">
                                    <div className="card-image">
                                        <img src={sala.img} style={{ height: 190, width: 100 }} alt={sala.title} />
                                    </div>
                                    <div className="card-stacked">
                                        <div className="card-content">
                                        <span className="card-title">{sala.title}</span>
                                        <p>{sala.desc}</p>
                                        </div>
                                        <div className="card-action">
                                            <Link to={`/chat?sala=${sala.title}`}>INGRESAR</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          )  
                        })
                    }
                    </div>
                </div>
            </div>
        )
    }
}
