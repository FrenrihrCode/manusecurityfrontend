import React, { Component } from 'react'
import * as api from '../api/api';

class gestionSala extends Component{
    constructor(){
        super();
        this.state={
            title:'',
            desc:'',
            user:'',
            img:'',
            salas:[],
        };
        this.addSala = this.addSala.bind(this);
        this.textOnchange = this.textOnchange.bind(this);
    }

    async addSala(e){
       
       if(this.state._id){
            try{
                const payload = { title: this.state.title, desc: this.state.desc,user: this.state.user,img: this.state.img  };
                const { data } = await api.reportesput(this.state._id,payload);
                console.log(data);
            } catch (error) {
                console.log("error" + error);
            }
        }else{
            try{
                const payload = { title: this.state.title, desc: this.state.desc,user: this.state.user,img: this.state.img  };
                const { data } = await api.reportes(payload);
                console.log(data);
            } catch (error) {
                console.log("error" + error);
            }
        }
    
       e.preventDefault();
    }

    async listarSala(){
        try{
            const {data} = await api.reporteslistar();
            this.setState({salas: data});
            console.log(data);
        } catch (error) {
            console.log("error" + error);
        }
    }

   async deleteSala(id){
        if(window.confirm('Estas seguro de eliminarlo')){

            try{
                const { data } = await api.reportesdelete(id);
                this.listarSala();
                console.log(data);
            } catch (error) {
                console.log("error" + error);
            }
        }
    }
   async editSala(id) {

        try{
            const { data } = await api.reportesget(id);
            this.setState({
                title: data.title,
                desc: data.desc,
                img: data.img,
                user: data.user,
                _id: data._id
            });
            console.log(data);

        } catch (error) {
            console.log("error" + error);
        }
    }


   componentDidMount(){
        this.listarSala();
    }

    textOnchange(e){
        const {name, value} = e.target
        this.setState({
            [name]: value
        })
    }

    render(){
        return(
            <div className="row">
                <div className="col s3">
                    <div className="card">
                        <div className="card-content">
                            <form onSubmit={this.addSala}>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input id= "firstname" className="validate" required name="title" onChange={this.textOnchange} value={this.state.title} type="text" placeholder="Ingrese nombre"></input>
                                    </div>
                                    <div className="input-field col s12">
                                        <input className="validate" required name="desc" onChange={this.textOnchange} value={this.state.desc} type="text" placeholder="Ingrese descripcion"></input>
                                    </div>
                                    <div className="input-field col s12">
                                        <textarea required name="user" onChange={this.textOnchange} type="text" value={this.state.user} placeholder="Aqui esta el propietario" className="materialize-textarea validate"></textarea>
                                    </div>
                                    <div className="input-field col s12">
                                        <textarea required  name="img" onChange={this.textOnchange} type="text" value={this.state.img} placeholder="Ingrese URL imagen" className="materialize-textarea validate"></textarea>
                                    </div>
                                </div>
                                <button type="submit" className="btn light-blue darken-4">
                                    Enviar
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="col s9">
                    <table className="responsive-table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Propietario</th>
                                <th>Imagen</th>
                                <th>OPERACIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.salas.map(sala=>{
                                    return(
                                        <tr key={sala._id}>
                                            <td>{sala.title}</td>
                                            <td>{sala.desc}</td>
                                            <td>{sala.user}</td>
                                            <td>{sala.img}</td>
                                            <td>
                                                <button className="btn light-blue darken-4"
                                                onClick={()=> this.editSala(sala._id)}><i className="material-icons">edit</i></button>
                                                <button className="btn light-blue darken-4"
                                                style={{margin:'4px'}}
                                                onClick={()=>this.deleteSala(sala._id)} ><i className="material-icons">delete</i></button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}
export default gestionSala;