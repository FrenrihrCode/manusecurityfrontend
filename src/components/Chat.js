import React, { useState, useEffect, useRef } from 'react';
import queryString from 'query-string';
import io from "socket.io-client";
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import { Redirect } from 'react-router-dom';
import { isAuthed } from "../api/tokenUtils";
import {getUser} from '../api/api'
import './Message.css';
let socket;

export default function Chat() {
    const [redirect, setRedirect] = useState(false);
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [users, setUsers] = useState([]);
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const [typing, setTyping] = useState(false);
    const [whostyping, setWhostyping] = useState("");
    const ENDPOINT = 'https://practica-04-dawa.herokuapp.com/';
    const messagesEndRef = useRef(null);

    const scrollToBottom = () => {
        messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    };

    useEffect(scrollToBottom, [messages]);

    useEffect(() => {
        socket = io(ENDPOINT);
        const authed = isAuthed()
        if(authed){
            try{
                obtenerUser(authed).then((response)=>{
                    setName(response.username)
                    let { sala } = queryString.parse(window.location.search);
                    setRoom(sala);
                    socket.emit('join', { name: response.username, room: sala }, (error) => {
                        if (error) {
                            alert(error);
                            setRedirect(true);
                        }
                    });
                })
            } catch (error) {
                console.log("error" + error);
            }
        } else {
            setRedirect(true);
        }
        // CLEAN UP THE EFFECT
        return () => socket.disconnect();
    }, [ENDPOINT]);

    useEffect(() => {
        console.log('use effect para mensajes');
        socket.on('message', message => {
            console.log(message);
            setMessages((oldMessages) => [...oldMessages, message]);
        });

        socket.on("roomData", ({ users }) => {
            setUsers(users);
        });

        socket.on('display', (data) => {
            if (data.typing === true) {
                setTyping(true);
                setWhostyping(data.user);
            } else {
                setTyping(false);
            }
        });
    }, []);

    const sendMessage = (event) => {
        event.preventDefault();
        if (message) {
            socket.emit('sendMessage', message, () => setMessage(''));
            socket.emit('typing', { user: name, typing: false });
        }
    }

    const userIsTyping = (event) => {
        setMessage(event.target.value);
        socket.emit('typing', { user: name, typing: true });
    }

    const obtenerUser = async(userId)=>{
        try{
            return await getUser(userId)
                .then((response) => {
                    return response.data
                });
        } catch (error) {
            console.log("error" + error);
        }
    }
    
    return (
        redirect ? <Redirect to='/salas' />
            : <div className="container ">
                <div className="row">
                    <div className="col s4">
                        <div className="card darken-1" style={{ padding: 10 }}>
                        <ul className="collection">
                            <Card.Header as='h5'>Usuarios en la sala {room}</Card.Header>
                            {users ?
                                <ListGroup variant='flush'>
                                    {users.map(({ name, i }) => (
                                        <li key={name} className="collection-item"> 
                                            <ListGroup.Item>
                                                <i className="green-text tiny material-icons">verified_user</i>{name}
                                            </ListGroup.Item>
                                        </li>
                                    ))}
                                </ListGroup>
                                :
                                null
                            }
                            </ul>
                        </div>
                    </div>
                    <div className="col s8">
                        <div className="card darken-1">
                            <span className="card-title"  >
                                <div style={{ paddingLeft: 20, paddingTop: 20 }}>
                                    Chat de la sala {room}
                                </div>
                                {typing ?
                                        <div>
                                            <span className="badge">{whostyping} esta escribiendo...</span>
                                        </div>
                                    : null
                                }
                            </span>
                            <div className="card-content ">
                                <ListGroup style={{ height: 400, paddingTop: 10, paddingBottom: 10, overflow: 'scroll' }}>
                                    {messages.map((message, i) => (
                                        name === message.user ?
                                            <div key={i} className="messageContainer justifyEnd">
                                                <div>
                                                    <i className="medium material-icons prefix">account_circle</i>
                                                    <div className="messageBox backgroundBlue">
                                                        <p className="messageText colorWhite"> {message.text}</p>
                                                    </div>
                                                    <p className="sentText pr-10 justifyEnd"> {message.hora} </p>
                                                </div>
                                            </div>
                                            : <div key={i} className="messageContainer justifyStart">
                                                <div>
                                                    <div className="messageBox backgroundLight">
                                                        <p className="messageText colorDark"> {message.text}</p>
                                                    </div>
                                                    <i className="medium material-icons prefix">account_circle</i>
                                                    <p className="sentText pl-10 ">{message.user} {message.hora} </p>
                                                </div>
                                            </div>
                                    ))}
                                    <div ref={messagesEndRef} />
                                </ListGroup>
                            </div>
                            <div className="card-action">
                                <form>
                                    <div className="row">
                                        <div className="col s8">
                                            <div className="input-field col s12">
                                                <i className="material-icons prefix">message</i>
                                                <input id="icon_telephone" value={message} type="tel" onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
                                                    className="validate" onChange={event => userIsTyping(event)} />
                                                <label htmlFor="icon_telephone">Mensaje</label>

                                            </div>

                                        </div>
                                        <div className="right-center">
                                            <button className="btn-floating btn-large waves-effect waves-light blue" onClick={e => sendMessage(e)} >
                                                <i className="material-icons">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}
