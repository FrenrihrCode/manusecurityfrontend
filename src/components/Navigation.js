import React from "react";
import { Link } from "react-router-dom";

const Navigation = () => (
    <>
    <nav>
        <div className="nav-wrapper grey darken-4">
            <div className="brand-logo">
            <Link to="/" style={{ paddingLeft: 15 }}>
                ChatApp
            </Link>      
            </div>
            <Link to="/" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></Link>
            <ul className="right hide-on-med-and-down">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/login">Ingresar</Link></li>
                <li><Link to="/register">Registrarse</Link></li>
            </ul>
        </div>
    </nav>

    <ul className="sidenav" id="mobile-demo">
        <li><Link to="/">Home</Link></li>
        <li><Link to="/login">Ingresar</Link></li>
        <li><Link to="/register">Registrarse</Link></li>
    </ul>
</>
);

export default Navigation;