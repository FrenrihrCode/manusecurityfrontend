import React from 'react';
import './footer.scss';

const Footer = () => {
  return (
    <div className="footer">
      <ul className="footer__ul leftSide">
        <li className="footer__li">Proyectos</li>
        <li className="footer__li">Contribuidores</li>
        <li className="footer__li">Registrarse</li>
      </ul>
      <ul className="footer__ul centerSide">
        <li className="footer__li">All Right Reserve</li>
      </ul>
      <div className="footer__ul rightSide">
        <div className="footer__icons">
        </div>
        <div className="footer__goUp">
        </div>
      </div>
    </div>
  );
};
export default Footer;